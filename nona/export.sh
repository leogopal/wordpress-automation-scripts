#!/bin/bash
source ~/.nona/_lib/utilities.sh
source ~/.nona/_lib/config.sh
source ~/.nona/_lib/seed_utilities.sh
source ~/.nona/_lib/vhost.sh
source ~/.nona/_lib/features.sh

do_assets_export() {
  if
    [[ ${HAS_COMPOSER} == 1 ]]
  then
    composer config github-oauth.github.com ${GITHUB_OAUTH_KEY} && \
    composer install
  fi

  cd assets

  if
    [[ ${HAS_NPM} == 1 ]]
  then
    rm -rf node_modules && npm install && npm update && npm rebuild node-sass
  fi

  if
    [[ ${HAS_BOWER} == 1 ]]
  then
    rm -rf bower_components && bower install 
  fi

  if
    [[ ${HAS_GRUNT} == 1 ]]
  then
    grunt buildit
  fi

  cd ..
}

create_uploads_folder() {
  mkdir -p ${WP_PATH}/wp-content/upload
  chmod 775 ${WP_PATH}/wp-content/upload
}

check_uploads_folder_exists() {
  if
    [ -d ${WP_PATH}/wp-content/upload ]
  then
    output "Uploads folder exists\n"
  else
    output "Uploads folder does not exist, attempting to create\n"
    create_uploads_folder
  fi
}

do_export() {
  # cleanup previous export
  rm -rf /tmp/wp_export
  # compress current theme
  cd ${WP_PATH}/wp-content/themes
  tar czf /tmp/${THEME_NAME}_export.tar ./${THEME_NAME}
  mkdir -p ${WP_PATH}/wp-content/themes/${THEME_NAME}/_export
  # move to a temp folder
  cd /tmp
  mkdir -p wp_export
  cd wp_export
  # download a fresh wp install
  wp core download --path=${THEME_NAME} >/dev/null 
  cd ${THEME_NAME}

  # create an .htaccess (indentation must be like this)

cat > .htaccess <<EOL
  <IfModule mod_rewrite.c>
  RewriteEngine On
  RewriteBase /
  RewriteRule ^index\.php$ - [L]
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteCond %{REQUEST_FILENAME} !-d
  RewriteRule . /index.php [L]
  </IfModule>
EOL

  info "Configuring...\n"
  # configure with *current* user details
  wp core config --dbname=${IMPORT_DB_NAME} --dbuser=${IMPORT_DB_USER} --dbpass=${IMPORT_DB_PASSWORD} --dbhost=${IMPORT_DB_HOST}
  # install the theme
  cd wp-content/themes
  mv /tmp/${THEME_NAME}_export.tar .
  tar xzf ${THEME_NAME}_export.tar
  cd ${THEME_NAME}
  # build the assets
  info "Exporting Assets. This may take a while...\n"
  do_assets_export > /dev/null 2>&1
  info "Asset export complete\n"
  # activate the theme
  wp theme activate ${THEME_NAME}
  # export seed file
  export_processed_seed
  # install and activate plugins
  info "Downloading and configuring plugins. This may take a while...\n"
  nonawp install-plugins --force >/dev/null 2>&1
  wp plugin activate --all
  info "Plugin setup complete\n"
  # set correct file permissions
  info "Setting correct permissions\n"
  find . -type d -exec chmod 755 {} +
  find . -type f -exec chmod 644 {} +
  # change config to new details
  rm ../../../wp-config.php
  info "Configuring exported site\n"
  wp core config --dbname=${EXPORT_DB_NAME} --dbuser=${EXPORT_DB_USER} --dbpass=${EXPORT_DB_PASSWORD} --dbhost=${EXPORT_DB_HOST}
  # cleanup
  info "Cleaning up...\n"
  cleanup_export
  cd ..
  info "Generating Archive\n"
  rm -rf twentyfifteen twentyfourteen twentysixteen
  rm ${THEME_NAME}_export.tar
  cd /tmp
  mv wp_export/${THEME_NAME} .
  tar czf ${THEME_NAME}_exported_site.tar ./${THEME_NAME}
  zip -r -q ${THEME_NAME}_exported_site.zip ./${THEME_NAME}
  mv ${THEME_NAME}_exported_site.tar ${THIS_DIR}/_export
  mv ${THEME_NAME}_exported_site.zip ${THIS_DIR}/_export

}

get_import_variables() {
  IMPORT_DB_NAME=$(wp eval 'echo DB_NAME;')
  IMPORT_DB_USER=$(wp eval 'echo DB_USER;')
  IMPORT_DB_PASSWORD=$(wp eval 'echo DB_PASSWORD;')
  IMPORT_DB_HOST=$(wp eval 'echo DB_HOST;')
}

get_export_variables() {
  read -p "New Database Name (${THEME_NAME}): " EXPORT_DB_NAME
  if [[ ${EXPORT_DB_NAME} == '' ]]; then
    EXPORT_DB_NAME=${THEME_NAME}
    info "Using ${EXPORT_DB_NAME}\n"
  fi

  read -p "New Databaser User: " EXPORT_DB_USER
  if [[ ${EXPORT_DB_USER} == '' ]]; then
    warn "Must provide a DB user\n"
    die "Exiting\n"
  fi

  read -p "New Database Password: " EXPORT_DB_PASSWORD
  if [[ ${EXPORT_DB_PASSWORD} == '' ]]; then
    info "Using empty password!\n"
  fi

  read -p "New Database Host (localhost): " EXPORT_DB_HOST
  if [[ ${EXPORT_DB_HOST} == '' ]]; then
    EXPORT_DB_HOST="localhost"
    info "Using localhost\n"
  fi

  read -p "New Site URL: " EXPORT_NEW_URL
  if [[ ${EXPORT_NEW_URL} == '' ]]; then
    warn "Must provide a new URL\n"
    die "Exiting\n"
  fi

  verify_domain ${EXPORT_NEW_URL}
}

cleanup_export() {
  rm -rf .git/
  rm -rf _seed/ && \
  rm -rf assets/ && \
  rm -f .gitignore && \
  rm -f *.sh && \
  rm -f *.md && \
  rm -f auth.json && \
  rm -f composer.json && \
  rm -f composer.phar && \
  rm -f composer.lock && \
  rm -f wp-plugins.json && \
  rm -f config && \
  rm -f config.sample
}

export_processed_seed() {
  cd _seed
  dev_domain_choice=${EXPORT_NEW_URL}
  check_seed_exists
  extract_seed_gzip
  verify_seed_variables
  info "Exporting database with new settings\n"
  substitute_dev_domain_serialized >/dev/null 2>&1
  substitute_dev_path_serialized >/dev/null 2>&1
  substitute_dev_domain
  substitute_dev_path
  mv temp.sql ../${THEME_NAME}_exported.sql
  cd ..
  cp ${THEME_NAME}_exported.sql ${THIS_DIR}/_export
  rm -rf _seed
}

info "Exporting ${THEME_NAME} project to portable archive\n\n"

THIS_DIR=$(pwd)

import_config;
get_wordpress_path;
get_import_variables;
get_export_variables;
info "Discovering project setup\n"
get_features;
info "Preparing to export...\n"
do_export;
cleanup_export;

info "SUCCESS\n\n"

exit 0

