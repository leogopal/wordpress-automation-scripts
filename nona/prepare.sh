#!/bin/bash
source ~/.nona/_lib/utilities.sh
source ~/.nona/_lib/config.sh
source ~/.nona/_lib/vhost.sh
source ~/.nona/_lib/features.sh

do_assets() {
  if
    [[ ${HAS_COMPOSER} == 1 ]]
  then
    composer config github-oauth.github.com ${GITHUB_OAUTH_KEY} && \
    composer install
  fi

  cd assets
  rm -rf node_modules bower_components

  if
    [[ ${HAS_NPM} == 1 ]]
  then
    npm install && npm update
  fi

  if
    [[ ${HAS_BOWER} == 1 ]]
  then
    bower install
  fi

  if
    [[ ${HAS_GRUNT} == 1 ]]
  then
    grunt buildit
  fi

  cd ..
}

create_uploads_folder() {
  mkdir ${WP_PATH}/wp-content/upload
  chmod 775 ${WP_PATH}/wp-content/upload
}

check_uploads_folder_exists() {
  if
    [ -d ${WP_PATH}/wp-content/upload ]
  then
    output "Uploads folder exists\n"
  else
    output "Uploads folder does not exist, attempting to create\n"
    create_uploads_folder
  fi
}

get_features

import_config

get_wordpress_path
check_uploads_folder_exists

# All good, let's do this

info "Preparing ${THEME_NAME} project\n\n"

do_assets

info "SUCCESS\n\n"

exit 0

