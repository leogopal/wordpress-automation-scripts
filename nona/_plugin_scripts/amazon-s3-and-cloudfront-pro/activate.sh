#!/bin/bash
source ~/.nona/_lib/utilities.sh
source ~/.nona/_lib/config.sh

config_s3_offload_plugin() {
  # take the sql file
  $(cat ~/.nona/_plugin_scripts/amazon-s3-and-cloudfront-pro/offload_3s_script.sql | \
  # replace the bucket name _length_ for the serial array
  sed "s/##S3_BUCKET_LENGTH##/${#AMAZON_S3_BUCKET}/g" | \
  # replace the bucket name
  sed "s/##S3_BUCKET_NAME##/${AMAZON_S3_BUCKET}/g" | \
  # insert the license key
  sed "s/##OFFLOAD_S3_LICENSE_KEY##/${OFFLOAD_S3_LICENSE}/g" | \
  # use correct table prefix
  sed "s/##WP_TABLE_PREFIX##/${WP_TABLE_PREFIX}/g" | \
  # run that query
  wp db query
  )
}

check_plugin_exists() {
  if
    # check if we have the aws pplugin
    [[ $(wp plugin list  | grep '^amazon-s3-and-cloudfront-pro\s') ]]
  then
    printf "Offload S3 is present, registering\n\n"
  else
    printf "Offload S3 is not present, skipping configuration\n\n"
  fi
  exit 0
}

output "Checking for Offload S3 Plugin\n"
import_config 1>/dev/null
check_plugin_exists
config_s3_offload_plugin
