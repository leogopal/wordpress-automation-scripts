DELETE FROM `##WP_TABLE_PREFIX##_options` WHERE `option_name` IN ("rg_gforms_key", "rg_gforms_disable_css", "rg_gforms_enable_html5", "rg_gforms_captcha_public_key", "rg_gforms_captcha_private_key", "rg_gforms_currency", "rg_gforms_message", "gform_pending_installation", "gform_enable_background_updates", "gform_longtext_ready", "rg_gforms_enable_akismet", "_transient_gform_update_info", "_transient_timeout_gform_update_info", "widget_gform_widget");

INSERT INTO `##WP_TABLE_PREFIX##_options` (`option_name`, `option_value`, `autoload`) VALUES ("rg_gforms_key", "##GRAVITY_FORMS_REGISTRATION_KEY##", "yes");
INSERT INTO `##WP_TABLE_PREFIX##_options` (`option_name`, `option_value`, `autoload`) VALUES ("rg_gforms_disable_css", "1", "yes");
INSERT INTO `##WP_TABLE_PREFIX##_options` (`option_name`, `option_value`, `autoload`) VALUES ("rg_gforms_enable_html5", "0", "yes");
INSERT INTO `##WP_TABLE_PREFIX##_options` (`option_name`, `option_value`, `autoload`) VALUES ("rg_gforms_captcha_public_key", "", "yes");
INSERT INTO `##WP_TABLE_PREFIX##_options` (`option_name`, `option_value`, `autoload`) VALUES ("rg_gforms_captcha_private_key", "", "yes");
INSERT INTO `##WP_TABLE_PREFIX##_options` (`option_name`, `option_value`, `autoload`) VALUES ("rg_gforms_currency", "ZAR", "yes");
INSERT INTO `##WP_TABLE_PREFIX##_options` (`option_name`, `option_value`, `autoload`) VALUES ("rg_gforms_message", "<!--GFM-->", "yes");
INSERT INTO `##WP_TABLE_PREFIX##_options` (`option_name`, `option_value`, `autoload`) VALUES ("gform_enable_background_updates", "0", "yes");
INSERT INTO `##WP_TABLE_PREFIX##_options` (`option_name`, `option_value`, `autoload`) VALUES ("gform_longtext_ready", 1, "yes");
INSERT INTO `##WP_TABLE_PREFIX##_options` (`option_name`, `option_value`, `autoload`) VALUES ("rg_gforms_enable_akismet", "0", "yes");
INSERT INTO `##WP_TABLE_PREFIX##_options` (`option_name`, `option_value`, `autoload`) VALUES ("widget_gform_widget", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `##WP_TABLE_PREFIX##_options` (`option_name`, `option_value`, `autoload`) VALUES ("gform_pending_installation", "0", "yes");

