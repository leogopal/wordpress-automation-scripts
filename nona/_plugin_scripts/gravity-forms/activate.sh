#!/bin/bash
source ~/.nona/_lib/utilities.sh
source ~/.nona/_lib/config.sh

check_plugin_exists() {
  if
    # check if we have the aws pplugin
    [[ $(wp plugin list  | grep '^gravityforms\s') ]]
  then
    printf "Gravity Forms is present, registering\n\n"
  else
    printf "Gravity Forms plugin is not present, skipping key installation\n\n"
  fi
  exit 0
}

install_gravity_license() {
  $(cat ~/.nona/_plugin_scripts/gravity-forms/gravity_forms_license_script.sql | \
  # replace the key
  sed "s/##GRAVITY_FORMS_REGISTRATION_KEY##/${GRAVITY_FORMS_REGISTRATION_KEY}/g" | \
  # use correct table prefix
  sed "s/##WP_TABLE_PREFIX##/${WP_TABLE_PREFIX}/g" | \
  # run that query
  wp db query
  )
}

output "Checking for Gravity Forms Plugin\n"
import_config 1>/dev/null
check_plugin_exists
install_gravity_license
