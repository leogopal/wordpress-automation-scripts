#!/bin/bash
source ~/.nona/_lib/utilities.sh
source ~/.nona/_lib/config.sh

check_plugin_exists() {
  if
    # check if we have the aws pplugin
    [[ $(wp plugin list  | grep '^wp-migrate-db\s') ]]
  then
    output "Migrate DB plugin is present, configuring\n\n"
  else
    output "Migrate DB Plugin doesn't exist\n"
    exit 0
  fi
}

activate_wp_db_plugin() {
  get_site_url_without_protocol
  get_wordpress_path
  HTTPS_URL=https://${SITE_URL_WITHOUT_PROTOCOL}
  HTTP_URL=http://${SITE_URL_WITHOUT_PROTOCOL}
  EXPORT_PROFILE_NAME=${THEME_NAME}-db-seed-export
  RANDOM_KEY=$(generate_random_string 40)

  wp db query "DELETE FROM ${WP_TABLE_PREFIX}_options WHERE option_name = 'wpmdb_settings';"
  # ${FOO} with a # in it - ${#FOO} - gives the length of the variable
  wp db query "INSERT INTO ${WP_TABLE_PREFIX}_options (option_name, option_value) VALUES ('wpmdb_settings', 'a:11:{s:3:\"key\";s:40:\"${RANDOM_KEY}\";s:10:\"allow_pull\";b:0;s:10:\"allow_push\";b:0;s:8:\"profiles\";a:1:{i:0;a:18:{s:13:\"save_computer\";s:1:\"1\";s:9:\"gzip_file\";s:1:\"1\";s:13:\"replace_guids\";s:1:\"0\";s:12:\"exclude_spam\";s:1:\"1\";s:19:\"keep_active_plugins\";s:1:\"0\";s:13:\"create_backup\";s:1:\"0\";s:18:\"exclude_post_types\";s:1:\"1\";s:18:\"exclude_transients\";s:1:\"1\";s:25:\"compatibility_older_mysql\";s:1:\"1\";s:6:\"action\";s:8:\"savefile\";s:15:\"connection_info\";s:0:\"\";s:11:\"replace_old\";a:3:{i:1;s:${#HTTP_URL}:\"${HTTP_URL}\";i:2;s:${#HTTPS_URL}:\"${HTTPS_URL}\";i:3;s:${#WP_PATH}:\"${WP_PATH}\";}s:11:\"replace_new\";a:3:{i:1;s:11:\"##DEV_URL##\";i:2;s:11:\"##DEV_URL##\";i:3;s:15:\"##DEV_WP_PATH##\";}s:22:\"save_migration_profile\";s:1:\"1\";s:29:\"save_migration_profile_option\";s:3:\"new\";s:18:\"create_new_profile\";s:${#EXPORT_PROFILE_NAME}:\"${EXPORT_PROFILE_NAME}\";s:17:\"select_post_types\";a:1:{i:0;s:8:\"revision\";}s:4:\"name\";s:${#EXPORT_PROFILE_NAME}:\"${EXPORT_PROFILE_NAME}\";}}s:7:\"licence\";s:0:\"\";s:10:\"verify_ssl\";b:0;s:17:\"blacklist_plugins\";a:0:{}s:11:\"max_request\";i:1048576;s:22:\"delay_between_requests\";i:0;s:18:\"prog_tables_hidden\";b:1;s:21:\"pause_before_finalize\";b:0;}');"

  output "Migrate DB Profile Added\n"
}

output "Checking for Migrate DB Plugin\n"
import_config 1>/dev/null

check_plugin_exists
activate_wp_db_plugin
