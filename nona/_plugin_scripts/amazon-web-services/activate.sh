#!/bin/bash
source ~/.nona/_lib/utilities.sh
source ~/.nona/_lib/config.sh

install_aws_keys() {
  $(
  # echo the script containing replacement variables
  cat ~/.nona/_plugin_scripts/amazon-web-services/aws_key_script.sql | \
  # perform the replacements
  sed "s/##AWS_KEY##/${AWS_KEY}/g" | \
  sed "s/##AWS_SECRET##/${AWS_SECRET}/g" | \
  # use correct table prefix
  sed "s/##WP_TABLE_PREFIX##/${WP_TABLE_PREFIX}/g" | \
  # and run the query
  wp db query
  )
}

check_plugin_exists() {
  if
    # check if we have the aws pplugin
    [[ $(wp plugin list  | grep '^amazon-web-services\s') ]]
  then
    printf "AWS is present, registering\n\n"
  else
    printf "AWS plugin is not present, skipping key installation\n\n"
  fi
  exit 0
}

output "Checking for AWS Plugin\n"
import_config 1>/dev/null
check_plugin_exists
install_aws_keys
