#!/bin/bash

check_seed_exists() {
  PATTERN=[a-zA-Z0-9]*\-migrate\-[0-9]*\.sql\.gz
  message "Checking for ${THEME_NAME} seed file "
  if
    ls -la | grep ${PATTERN} 1>/dev/null
  then
    if
      ! [ $(find . -maxdepth 2 -name "${PATTERN}" | wc -l) -eq 1 ]
    then
      fail
      info "\nThere is more than one seed file!\n\n${white}Please ensure there is only one .sql.gz file that matches the pattern.\n"
      die "${red}Aborting...${clear_colour}\n"
    else
      ok
    fi
  else
    fail
    info "\nThe seed file does not exist.\n\nExpecting file to match pattern ${white}${PATTERN}"
    info "\nThis file needs to be in your projects _seed directory\n\n"
    die "${red}Aborting...${clear_colour}\n"
  fi
}

extract_seed_gzip() {
  message "Decompressing archive... "
  FILENAME=$(find . -maxdepth 1 -name [a-zA-Z0-9]*-migrate-[0-9]*.sql.gz)
  gunzip ${FILENAME} -c > temp.sql
  ok
}

verify_seed_variables() {
  message "Verifying seed file... "
  if
    grep "##DEV_URL##" temp.sql 1>/dev/null
  then
    ok
  else
    fail
    info "\n${white}The seed file is not properly formatted. Expecting replacement variable ${blue}##DEV_URL##${white} but none found.\n\n"
    die "${red}Aborting...${clear_colour}\n\n"
  fi
}

substitute_dev_path_serialized() {
  message "Replacing paths in serialized arrays \n"
  # todo needs to handle all valid url characters like numbers
  OLDIFS=${IFS}
  IFS=$(echo -en "\n\b")
  for i in $(cat temp.sql| grep 's\:[0-9]*\:\"[^;]*##DEV_WP_DIR##[^;]*";' -o);
  do
    CURRENT_STRING=$(echo ${i} | cut -d \" -f2-99 | sed 's~\";$~~')
    NEW_STRING=$(echo ${CURRENT_STRING} | sed "s~\#\#DEV_WP_DIR\#\#~${WP_PATH}~g")
    REPLACE="s:${#NEW_STRING}:\"${NEW_STRING}\";"
    sed -i.bak "s~${i}~${REPLACE}~" temp.sql
    info "Replaced ${i} \n    with ${REPLACE}\n\n"
  done
  IFS=${OLDIFS}
}

substitute_dev_domain_serialized() {
  message "Replacing domains in serialized arrays \n"
  # todo needs to handle all valid url characters like numbers
  OLDIFS=${IFS}
  IFS=$(echo -en "\n\b")
  for i in $(cat temp.sql| grep 's\:[0-9]*\:\"[^;]*##DEV_URL##[^;]*";' -o);
  do
    CURRENT_STRING=$(echo ${i} | cut -d \" -f2-99 | sed 's~\";$~~')
    NEW_STRING=$(echo ${CURRENT_STRING} | sed "s~\#\#DEV_URL\#\#~${dev_domain_choice}~g")
    REPLACE="s:${#NEW_STRING}:\"${NEW_STRING}\";"
    sed -i.bak "s~${i}~${REPLACE}~" temp.sql
    info "Replaced ${i} \n    with ${REPLACE}\n\n"
  done
  IFS=${OLDIFS}
}

substitute_dev_domain() {
  message "Setting dev domain to ${dev_domain_choice} "
  # change url to dev url
  # using ~ as the sed delimiter as we are using slashes in the domains
  sed -i.bak "s~\#\#DEV_URL\#\#~${dev_domain_choice}~g" temp.sql
  ok
}

substitute_dev_path() {
  message "Setting dev path to ${WP_DIR} "
  # change url to dev url
  # using ~ as the sed delimiter as we are using slashes in the domains
  sed -i.bak "s~\#\#DEV_WP_DIR\#\#~${WP_DIR}~g" temp.sql
  ok
}

