#!/bin/bash
source ~/.nona/_lib/utilities.sh

get_remote_version() {
  # must make token configurable
  REMOTE_VERSION=$(curl --header 'Authorization: token 790da254b07341f9064a455155b66105a13f30b6' \
      --header 'Accept: application/vnd.github.v3.raw' \
      --location https://api.github.com/repos/Nona-Creative/nona-wordpress-cli/contents/nona/VERSION 2>/dev/null)
}

get_local_version() {
  LOCAL_VERSION=$(cat ~/.nona/VERSION 2>/dev/null)
}

check_update_available() {
  get_remote_version
  get_local_version
  if
    [[ ${REMOTE_VERSION} != ${LOCAL_VERSION} ]]
  then
    return 0
  else
    return 1
  fi
}

check_update_needed() {
  if
    check_update_available
  then
    message "\nThere is a new version of this tool available.\n"
    info "Installed: ${LOCAL_VERSION} - Remote: ${REMOTE_VERSION}\n"
    warn "Update with the 'nonawp self-update' command\n\n"
    return 0
  else
    return 1
  fi
}

do_self_update() {
  output "Updating from ${LOCAL_VERSION} to ${REMOTE_VERSION}\n"
  if [ -e "$HOME/.nona" ]; then
    output "Removing your existing Nona installation\n"
    rm -rf "$HOME/.nona"
  fi
  output "Installing Nona WordPress CLI Tools\n"
  cd /tmp
  git clone git@github.com:Nona-Creative/nona-wordpress-cli.git 2>/dev/null
  cd nona-wordpress-cli
  cp -rf ./nona ~/.nona
  cd ..
  rm -rf /tmp/nona-wordpress-cli
  output "\nSuccessfully Updated to version ${REMOTE_VERSION}\n"
}


