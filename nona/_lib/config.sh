#!/bin/bash
source ~/.nona/_lib/wp-cli-utils.sh

check_valid_config() {
  message "Checking for config file"
  if
    [ -f ${CONFIG_PATH} ]
  then
    ok
  else
    fail && die "\nMissing config file. Aborting.\n"
    exit 1
  fi

  message "Checking if config file is valid"

  if
    [[ $(cat $1 | grep XXXXXXXX) ]]
  then
    fail && die "\nConfig doesn't seem valid. Have you replaced placeholder values? Aborting.\n"
    exit 0
  else
    ok
  fi
}

get_config_path() {
  get_wordpress_path
  get_full_wordpress_path
  CONFIG_PATH=${FULL_PATH}/config
}

import_config() {
  get_config_path
  check_valid_config ${CONFIG_PATH}
  for i in $(cat ${CONFIG_PATH})
  do
    export ${i}
  done
}
