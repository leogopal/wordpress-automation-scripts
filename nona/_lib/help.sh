#!/bin/bash

show_help() {
  info "Nona WordPress CLI Tool\n"
  output "Version $(cat ~/.nona/VERSION)\n\n"
  output "This tool is for managing Nona WordPress sites\n\n"
  output "Valid Commands\n\n"

  output "\t prepare \t\t Prepare a WordPress project. Useful after a fresh checkout.\n"
  output "\t install-plugins \t Installs the plugins defined in your wp-plugins.json file.\n"
  output "\t deploybot-template \t Generates a DeployBot deploy script based on your config.\n"
  output "\t seed-db \t\t Seeds your database from the _seed directory.\n"
  output "\t pagely-deploy \t\t Runs the pagely deployment script.\n"
  output "\t export \t\t Exports a portable version of the site.\n"
  output "\t self-update \t\t Check and update this tool.\n"
  output "\t generate-vhost \t Outputs a vhost template for you to use with Apache.\n"
  output "\t version \t\t Show the version of this tool.\n"
  output "\t help \t\t\t This screen.\n"

  output "\nOptions\n\n"

  info "install-plugins\n"
  output "\t --force \t\t Forces install to run without prompting for confirmation.\n"

  output "\n"
  info "seed-db\n"
  output "\t --domain \t\t Specify a domain to use. Removes need to prompt for hostname.\n"
  output "\t --force \t\t Forces a seed without prompting for confirmation.\n"

  output "\n"
  info "pagely-deploy\n"
  output "\t --seed \t\t Force a seed of the database in the _seed folder.\n"
  output "\t --cleanup \t\t Forces a cleanup of sensitive files. "
  warn "This is dangerous. Don't do this locally.\n"

  output "\n\nMore information is available at this tools github repo.\n"
}


