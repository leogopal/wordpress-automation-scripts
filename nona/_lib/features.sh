#!/bin/bash

HAS_COMPOSER=0
HAS_NPM=0
HAS_BOWER=0
HAS_GRUNT=0

get_features() {
  if
    [ -f ${FULL_PATH}/composer.json ]
  then
    output "Composer is present\n"
    HAS_COMPOSER=1
  fi

  if
    [ -f ${FULL_PATH}/assets/package.json ]
  then
    output "NPM is present\n"
    HAS_NPM=1
  fi

  if
    [ -f ${FULL_PATH}/assets/bower.json ]
  then
    output "Bower is present\n"
    HAS_BOWER=1
  fi

  if
    [ -f ${FULL_PATH}/assets/gruntfile.js ]
  then
    output "Grunt is present\n"
    HAS_GRUNT=1
  fi
}

