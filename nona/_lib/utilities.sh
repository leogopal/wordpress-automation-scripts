#!/bin/bash

# Colours
black='\e[0;30m'
red='\e[0;31m'
green='\e[0;32m'
yellow='\e[0;33m'
blue='\e[0;34m'
purple='\e[0;35m'
cyan='\e[0;36m'
white='\e[0;37m'
clear_colour='\e[0m'

####################################################################### Methods
# Quit with exit message
die() {
  printf >&2 "$@"
  exit 1
}

# Print OK Message
ok() {
  printf "${green} OK${clear_colour}\n"
}

# Print FAIL Message
fail() {
  printf "${red} FAIL${clear_colour}\n"
}

# Provide feedback
info() {
  printf "${cyan}$@${clear_colour}"
}

# Display a message
message() {
  printf "${blue}$@${clear_colour}"
}

output() {
  printf "${clear_colour}$@${clear_colour}"
}

warn() {
  printf "${red}$@${clean_colour}"
}

# Check dependencies
check_deps() {
  deps=(${1})

  printf "${blue}Checking required dependencies\n\n${clear_colour}"
  for i in "${deps[@]}"
  do
    printf "%-14s" "$i"
    if
      hash "$i" 2>/dev/null;
    then
      ok;
    else
      printf "${red}FAIL\n\n${red}Some dependencies are missing!\n\n${clear_colour}Please make sure you install them before trying again.\n\n"
      exit 1
    fi
  done
  printf "\n${green}Dependency check passed\n\n${clear_colour}"
}

generate_random_string() {
  cat /dev/urandom | env LC_CTYPE=C tr -dc 'a-zA-Z0-9' | fold -w ${@} | head -n 1
}

verify_domain() {
  if
    echo ${@} | grep '\(http\|https\)\:\/\/\([0-9A-Za-z]*\.\:\)*\([0-9A-Za-z]*\).*' 1>/dev/null
  then
    echo
  else
    die "${red}\nDomain is not formatted correctly. Only http and https domains are allowed.${clear_colour}\n"
  fi
}


