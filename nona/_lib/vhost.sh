#!/bin/bash
source ~/.nona/_lib/wp-cli-utils.sh

# use ~ as delimiter as / is in the paths
generate_vhost_template() {
  info "Copy the vhost template below\n"
  message "-------------------------------------------------------------\n\n"
  get_site_url_without_protocol
  # take the template file
  cat ~/.nona/vhost.template | \
  # replace the site address
  sed "s~##SITE_URL##~${SITE_URL_WITHOUT_PROTOCOL}~g" | \
  # replace the path name - %? strips the last character
  sed "s~##SITE_PATH##~${WP_PATH%?}~g" | \
  # insert the theme name
  sed "s~##SITE_NAME##~${THEME_NAME}~g"
}

