#!/bin/bash

get_wordpress_path() {
  ORIGINAL_PATH=${PWD}
  WP_PATH=$(wp eval 'echo ABSPATH;')
}

get_wordpress_theme() {
  THEME_NAME=$(wp theme list  | grep "\sactive" | cut -f 1)
  if
    [[ ${THEME_NAME} == '' ]]
  then
    warn "There are no active themes. Exiting\n"
    output "Try 'wp theme list' to see available themes, and 'wp theme activate <name>' to activate.\n"
    exit 1
  fi
}

get_full_wordpress_path() {
  get_wordpress_path
  get_wordpress_theme
  FULL_PATH=${WP_PATH}wp-content/themes/${THEME_NAME}
}

get_site_url() {
  SITE_URL=$(wp option get siteurl)
}

get_site_url_without_protocol() {
  SITE_URL_WITHOUT_PROTOCOL=$(wp option get siteurl | sed s/http\:\\/\\///g)
}

goto_wordpress_path() {
  FULL_PATH=${WP_PATH}wp-content/themes/${THEME_NAME}
  cd ${FULL_PATH} >/dev/null
}

goto_original_path() {
  cd ${ORIGINAL_PATH} >/dev/null
}

