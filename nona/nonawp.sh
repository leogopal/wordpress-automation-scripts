#!/bin/bash
source ~/.nona/_lib/utilities.sh
source ~/.nona/_lib/config.sh
source ~/.nona/_lib/wp-cli-utils.sh
source ~/.nona/_lib/github.sh
source ~/.nona/_lib/vhost.sh
source ~/.nona/_lib/help.sh

# You define the deps as space seperated in the string
DEPS="php jq curl node npm tar unzip wp composer bower npm"

SWITCH_PREPARE=0
SWITCH_INSTALL_PLUGINS=0
SWITCH_UPDATE=0
SWITCH_GENERATE_DEPLOYBOT_TEMPLATE=0
SWITCH_GENERATE_VHOST=0
SWITCH_PAGELY_DEPLOY=0
SWITCH_HELP=0
SWITCH_VERSION=0
SWITCH_UNKNOWN_COMMAND=0

SWITCH_FORCE=0
SWITCH_QUIET=0
SWITCH_DOMAIN=''

WP_PATH=''
ORIGINAL_PATH=''

THEME_NAME=''

get_switches() {
  SWITCH_FORCE=0

  options=$@

  # An array with all the arguments
  arguments=($options)

  # Loop index
  index=0

  for argument in $options
  do
    # Incrementing index
    index=`expr $index + 1`

    # The conditions
    case $argument in
      prepare) SWITCH_PREPARE=1 ;;
      install-plugins) SWITCH_INSTALL_PLUGINS=1 ;;
      seed-db) SWITCH_SEED_DB=1 ;;
      self-update) SWITCH_UPDATE=1 ;;
      deploybot-template) SWITCH_GENERATE_DEPLOYBOT_TEMPLATE=1 ;;
      pagely-deploy) SWITCH_PAGELY_DEPLOY=1 ;;
      generate-vhost) SWITCH_GENERATE_VHOST=1 ;;
      export) SWITCH_EXPORT_SITE=1 ;;
      help) SWITCH_HELP=1 ;;
      version) SWITCH_VERSION=1 ;;

      --force) SWITCH_FORCE=1 ;;
      --quiet) SWITCH_QUIET=1 ;;
      --verbose) SWITCH_VERBOSE=1 ;;
      --domain) SWITCH_DOMAIN=${arguments[index]} ;;
      --seed) SWITCH_SEED=1 ;;
      --cleanup) SWITCH_CLEANUP=1 ;;

      *) SWITCH_UNKNOWN_COMMAND=1 ;;
    esac
  done

  # if we have no arguments show help and exit
  if
    [[ ${index} == 0 ]]
  then
    show_help
    check_update_needed
    exit 1
  fi
}

do_prepare() {
  ~/.nona/prepare.sh
  exit 1
}

do_plugins() {
  goto_original_path
  if 
    [[ ${SWITCH_FORCE} == 1 ]]
  then
    ~/.nona/install-plugins.sh --force
  else
    ~/.nona/install-plugins.sh
  fi
  exit 1
}

do_seed_db() {
  SEED_SWITCHES=''

  if
    [[ ${SWITCH_DOMAIN} != '' ]]
  then
    SEED_SWITCHES+=" --domain $SWITCH_DOMAIN"
  fi

  if
    [[ ${SWITCH_FORCE} == 1 ]]
  then
    SEED_SWITCHES+=" --force"
  fi

  if
    [[ -d ${FULL_PATH}/_seed ]]
  then 
    cd ${FULL_PATH}/_seed
    ~/.nona/_seed/seed_db.sh ${SEED_SWITCHES}
    cd -
  fi
  exit 0
}

pagely_deploy() {
  DEPLOY_SWITCHES=''

  if
    [[ ${SWITCH_SEED} == 1 ]]
  then
    DEPLOY_SWITCHES+=" --seed"
  fi

  if
    [[ ${SWITCH_CLEANUP} == 1 ]]
  then
    DEPLOY_SWITCHES+=" --cleanup"
  fi

  ~/.nona/_deploy/pagely_deploy.sh ${DEPLOY_SWITCHES}

  exit 1
}

generate_deploybot_template() {
  ~/.nona/_deploy/generate_deploybot_script.sh

  exit 1
}

check_valid_location() {
  if
    [[ $(wp option get siteurl 2>/dev/null) ]]
  then
    hash -r
  else
    output "Not a valid WordPress installation. Aborting.\n"
    exit 1
  fi
}

export_site() {
  ~/.nona/export.sh
  exit 1
}

check_valid_location

get_switches ${@}

# shows the version of this tool
if
  [[ ${SWITCH_VERSION} == 1 ]]
then
  output "Nona WP CLI Tools, version $(cat ~/.nona/VERSION)\n\n"
  exit 1;
fi;

# allow a help flag to setup a project
if
  [[ ${SWITCH_HELP} == 1 ]]
then
  show_help
  exit 1;
fi;

# allow an update for this tool
if
  [[ ${SWITCH_UPDATE} == 1 ]]
then
  if
    check_update_available
  then
    do_self_update
  else
    output "Latest version is already installed, no update required.\n"
  fi
  exit 1;
fi;


if
  [[ ${SWITCH_QUIET} == 0 ]]
then
  check_update_needed
  import_config
  check_deps "${DEPS}"
fi;

get_wordpress_path
get_full_wordpress_path
goto_wordpress_path >/dev/null

# allow a prepare flag to setup a project
if
  [[ ${SWITCH_PREPARE} == 1 ]]
then
  do_prepare
fi;

# allow a install plugins flag to setup a project
if
  [[ ${SWITCH_INSTALL_PLUGINS} == 1 ]]
then
  do_plugins
fi;

# allow a seed flag to setup a project
if
  [[ ${SWITCH_SEED_DB} == 1 ]]
then
  do_seed_db
fi;

# allow a deploybot template generation switch
if
  [[ ${SWITCH_GENERATE_DEPLOYBOT_TEMPLATE} == 1 ]]
then
  generate_deploybot_template
fi;

# allow a deploybot template generation switch
if
  [[ ${SWITCH_PAGELY_DEPLOY} == 1 ]]
then
  pagely_deploy
fi;

if
  [[ ${SWITCH_GENERATE_VHOST} == 1 ]]
then
  generate_vhost_template
fi

if
  [[ ${SWITCH_EXPORT_SITE} == 1 ]]
then
  export_site
fi

if
  [[ ${SWITCH_UNKNOWN_COMMAND} == 1 ]]
then
  output "Invalid options. Try 'nonawp help' for more informantion\n"
  exit 1
fi

goto_original_path

exit 0
