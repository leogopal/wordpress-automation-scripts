#!/bin/bash
source ~/.nona/_lib/utilities.sh
source ~/.nona/_lib/config.sh

# Plugins are defined in wp-plugins.json

check_plugin_directory_exists() {
  message "Checking for WordPress plugin directory "
  cd ../..
  if
    ls -la 1> grep plugins 2>/dev/null
  then
    ok
    cd - > /dev/null
  else
    fail
    info "\nThe plugin directory is not available in ../../\n\n"
    die "${red}Aborting...${clear_colour}\n"
  fi
}

prompt_to_continue() {
  message "${white}IMPORTAINT\n\n${clear_colour}"
  info "This script will delete ALL plugins and install fresh versions of this projects dependencies.\n"
  message "${red}You have been warned!\n\n${clear_colour}"
  read -p "Continue? (Y/n) " choice
  if [ ${choice} = "Y" ]; then
    printf "Installing...\n"
  else
    die "Aborting installation\n"
  fi
}

pre_clean_plugin_folder() {
  message "Removing old plugins "
  cd ../../plugins
  rm -rf *
  cd - > /dev/null
  ok
}

clean_plugin_folder_archives() {
  cd ../../plugins
  if 
    ls -la | grep zip 1>/dev/null
  then
    info "Plugins archives exist, cleaning plugins\n"
    rm -rf *.zip 2>/dev/null
  else
    info "Plugin directory is clean\n"
  fi
  cd - > /dev/null
}

plugin_failed() {
  # status is not 200, fail and end installation
  fail "NO\n\n"; 

  # most likely cause is incorrect versions
  message "${@} ${clear_colour}does not exist\n\n${yellow}Please confirm that your wp-plugins.json is valid and that the versions you are requesting are available.\n\n";

  # remove the zip files
  rm -rf *.zip;

  # abort the installation script
  die "Aborting...\n"; 
}

download_public_plugins() {
  printf "\n${yellow}Downloading public plugins${clear_colour}\n\n"

  # loop through the plugins json file
  for i in $(cat wp-plugins.json | \

    # select the `public` section
    jq -r '.public ' | \

    # ignore the first and last line - { and }
    grep \" | \

    # take value from each line - right side of :
    cut -d ':' -f 1-2 | \

    # remove quotes that are followed by spaces and commas
    sed 's/[\"|\ \,]//g' | \

    # replace colons with periods
    sed 's/\:/\./g' | \

    # add `.zip` to the end of the string
    sed 's/$/\.zip/g' | \

    # add wordpress repo url to beginning of string
    sed 's/^/https\:\/\/downloads.wordpress.org\/plugin\//g' | \

    # if user has required `latest` version then remove term
    sed 's/\.latest//g'); 

  # perform action on the string
  do
    # output downloading message
    printf "Downloading\e[0;36m $i\e[0m"; 

    # go into plugins directory
    cd ../../plugins; 

    # do the curl and capture the status code
    STATUS=$(curl --fail -O --write-out "%{http_code}" $i 2>/dev/null); 

    # check to make sure status is 200
    if
      test $STATUS -ne 200; 
    then 
      plugin_failed $i;
    else
      # everything went well
      ok;
    fi;

    # go back to previous directory
    cd - > /dev/null; 
  done
}

download_premium_plugins() {
  printf "\n${yellow}Downloading premium plugins from Nona-Creative/wp-premium-plugins github repo${clear_colour}\n\n"

  # loop through json
  for i in $(cat wp-plugins.json | \
    # grab the `premium` block
    jq -r '.premium ' | \

    # ignore first and lat lines - { and }
    grep \" | \

    # take from right hand side of entry - after the :
    cut -d ':' -f 1-2 | \

    # replace quotes followed by spaces and commas
    sed 's/[\"|\ \,]//g' | \

    # prepend `/versions/` to the string
    sed 's/\(.*\)/\1\/versions\/\1/g' | \

    # replace colons with forward slashes
    sed 's/\:/\//g' | \

    # remove version number from middle of string
    sed 's/\/[0-9].*\/versions/\/versions/g' | \

    # remove latest from string if that is the requested version
    sed 's/\/latest\/versions/\/versions/g' | \

    # remove final slash to make zip file name correct
    sed 's/\(.*\)\//\1\./' | \

    # add .zip extension to end of string
    sed 's/$/\.zip/g' | \

    # add the premium repo url to the string
    sed 's/^/https\:\/\/api.github.com\/repos\/Nona-Creative\/wp-premium-plugins\/contents\//g');
  do
    cd ../../plugins;

    # get the tag - e.g. gravity-forms
    TAG=$(echo $i | cut -d '/' -f 8);
    # get the filename with version - e.g. gravity-forms.1.2.3.zip
    VERSION=$(echo $i | cut -d '/' -f 10);
    printf "Downloading\e[0;36m $VERSION \e[0m";

    # Pagely only has jq 1.2 installed, so we need to do the command slightly differently
    COMMAND="curl -H 'Authorization: token ${GITHUB_OAUTH_KEY}' https://api.github.com/repos/Nona-Creative/wp-premium-plugins/git/trees/master:${TAG}/versions 2>/dev/null | jq -r  '.tree' | jq -r '.[] | select(.path == \"${VERSION}\")' | jq -r '.url'"

    # now we need to get the url with the sha sig so we can get blobs greater than 1MB in size
    for a in $(eval ${COMMAND})
    do
      # assign the url to a var
      URL=$a;
    done

    # attempt the download and capture the status code
    STATUS=$(curl -H "Authorization: token ${GITHUB_OAUTH_KEY}" -H 'Accept: application/vnd.github.v3.raw' -L --fail -o ${VERSION} --write-out "%{http_code}" $URL 2>/dev/null);
    if ! [[ $STATUS -eq 200 ]];
    then
      plugin_failed $i;
    else
      # all good
      ok;
    fi;
    # go back to where we were
    cd - > /dev/null;
  done
}

download_custom_plugin_by_version() {
  # get the tag - e.g. gravity-forms
  TAG=${@}
  VERSION=$(cat wp-plugins.json | grep ${@} | cut -d ':' -f 2 | sed 's/"//g' | sed 's/,//g' | sed 's/\ //g');

  # get the filename with version - e.g. gravity-forms.1.2.3.zip
  printf "Downloading\e[0;36m ${TAG}.${VERSION}.zip \e[0m";
  cd ../../plugins;

  # Pagely only has jq 1.2 installed, so we need to do the command slightly differently
  COMMAND="curl -H 'Authorization: token ${GITHUB_OAUTH_KEY}' https://api.github.com/repos/Nona-Creative/wp-custom-plugins/git/trees/master:bin/${TAG}/versions 2>/dev/null | jq -r  '.tree' | jq -r '.[] | select(.path == \"${TAG}.${VERSION}.zip\")' | jq -r '.url'"

  # now we need to get the url with the sha sig so we can get blobs greater than 1MB in size
  for a in $(eval ${COMMAND})
  do
    # assign the url to a var
    URL=$a;
  done

  # attempt the download and capture the status code
  STATUS=$(curl -H "Authorization: token ${GITHUB_OAUTH_KEY}" -H 'Accept: application/vnd.github.v3.raw' -L --fail -o ${TAG}.${VERSION}.zip --write-out "%{http_code}" $URL 2>/dev/null);
  if ! [[ $STATUS -eq 200 ]];
  then
    # todo - handle renaming of plugin dirs
    plugin_failed $i;
  else
    # all good
    ok;
  fi;
  # go back to where we were
  cd - > /dev/null;
}

correct_custom_folder_name() {
  DIR_NAME=$(unzip -l ${@}.zip | head -5 | tail -1 | tr -s ' ' | cut -d ' ' -f5 | sed 's/\///g')
  unzip -oqq "${@}.zip" -d /tmp 
  mv /tmp/${DIR_NAME} ./${@}
  rm ${@}.zip
}

download_custom_plugin_by_url() {
  REAL_NAME=$(cat wp-plugins.json | jq -r '.custom' | grep \" | grep ${@} | cut -d "\"" -f4 | sed 's/https:\/\///g')
  printf "Downloading\e[0;36m ${@}\e[0m";
  cd ../../plugins;
  STATUS=$(curl -L --fail --write-out "%{http_code}" "https://${GITHUB_OAUTH_KEY}@${REAL_NAME}/archive/master.zip" --output ${@}.zip 2>/dev/null); 
  if ! [[ $STATUS -eq 200 ]];
  then
    plugin_failed $i;
  else
    correct_custom_folder_name ${@}
    ok;
  fi;
  cd - > /dev/null;
}

download_custom_plugins() {
  printf "\n${yellow}Downloading custom plugins${clear_colour}\n\n"

  for i in $(cat wp-plugins.json | jq -r '.custom' | grep \" | cut -d ':' -f 1 | sed 's/"//g' | sed 's/\ //g');
  do
    # check if this must come from a url or from a version
    if
      [[ $(cat wp-plugins.json | grep ${i} | grep 'github') ]]
    then
      download_custom_plugin_by_url ${i}
    else
      download_custom_plugin_by_version ${i}
    fi

  done
}

extract_plugins() {
  info "\nExtracting Plugins\n"
  cd ../../plugins
  pwd
  unzip -oqq '*.zip'
  cd - > /dev/null
}

finish() {
  info "\nSUCCESS\n\n"
  message "You can now go to wp-admin and activate the plugins\n\n"
  exit 0
}

get_switches() {
  SWITCH_FORCE=0

  options=$@

  # An array with all the arguments
  arguments=($options)

  # Loop index
  index=0

  for argument in $options
  do
    # Incrementing index
    index=`expr $index + 1`

    # The conditions
    case $argument in
      --force) SWITCH_FORCE=1 ;;
    esac
  done
}

get_switches ${@}

# do the things

# allow a `--force` flag that skips confirmation
if
  [[ ${SWITCH_FORCE} == 0 ]]
then
  prompt_to_continue
fi

output "Installing plugins, PWD is ${PWD}\n"

import_config >/dev/null

check_plugin_directory_exists
pre_clean_plugin_folder
download_public_plugins
download_premium_plugins
download_custom_plugins
extract_plugins
clean_plugin_folder_archives
finish

