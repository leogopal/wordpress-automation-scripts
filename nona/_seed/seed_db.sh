#!/bin/bash
source ~/.nona/_lib/utilities.sh
source ~/.nona/_lib/seed_utilities.sh
source ~/.nona/_lib/config.sh

capture_dev_domain() {
  info "\n"
  if 
    [[ ${SWITCH_DOMAIN} != 0 ]]
  then
    message "Domain set by --domain, value ${SWITCH_DOMAIN}\n"
    dev_domain_choice=${SWITCH_DOMAIN}
  else
    read -p "Your local dev environment URL (e.g. http://dev.my-site.com): " dev_domain_choice
  fi;

  verify_domain ${dev_domain_choice}
}

prompt_to_continue() {
  message "${white}IMPORTAINT\n\n${clear_colour}"
  info "This script will drop your existing schema for this application!\n"
  message "${red}You have been warned!\n\n${clear_colour}"
  read -p "Continue? (Y/n) " choice
  if [ ${choice} = "Y" ]; then
    printf "\n"
  else
    die "Aborting installation\n"
  fi
}

cleanup() {
  message "Cleaning up... "
  rm -rf temp.sql temp.sql.bak
  ok
}

finish() {
  wp theme activate ${THEME_NAME}
  info "\nSUCCESS\n"
  exit 0
}

import_config() {
  for i in $(cat ../config)
  do
    export ${i}
  done
}

check_valid_config() {
  cd ..
  message "Checking for config file"
  if 
    [ -f config ] 
  then
    ok
  else
    fail && die "\nMissing config file. Aborting.\n" 
  fi

  message "Checking if config file is valid"

  if
    [[ $(cat config | grep XXXXXXXX) ]]
  then
    fail && die "\nConfig doesn't seem valid. Have you replaced placeholder values? Aborting.\n" 
  else
    ok
  fi

  cd - 1>/dev/null
}

get_switches() {
  SWITCH_DOMAIN=0
  SWITCH_FORCE=0

  options=$@

  # An array with all the arguments
  arguments=($options)

  # Loop index
  index=0

  for argument in $options
  do
    # Incrementing index
    index=`expr $index + 1`

    # The conditions
    case $argument in
      --force) SWITCH_FORCE=1 ;;
      --domain) SWITCH_DOMAIN=${arguments[index]} ;;
    esac
  done
}

import_database() {
  message "Importing database "
  pwd
  if
    wp theme list | grep ${THEME_NAME} 1>/dev/null
  then
    ok
    wp db cli < temp.sql
  else
    fail
    die "\nwp cli tool is not present, or theme is incorrect, or this is not a Wordpress installation. Aborting\n"
  fi
}




get_switches ${@};

import_config;
check_valid_config;
get_wordpress_path;

# allow a `--force` flag that skips the prompt
if 
  [[ ${SWITCH_FORCE} == 0 ]]
then
  prompt_to_continue;
fi;

# # # do the things
check_deps;
check_seed_exists;
extract_seed_gzip;
verify_seed_variables;
capture_dev_domain;
substitute_dev_domain_serialized;
substitute_dev_path_serialized;
substitute_dev_domain;
substitute_dev_path;
import_database;
cleanup;
finish;
