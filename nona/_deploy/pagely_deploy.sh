#!/bin/bash
source ~/.nona/_lib/utilities.sh
source ~/.nona/_lib/config.sh

info "Deploying\n\n"

run_plugin_scripts() {
  info "Configuring Plugins\n\n"
  find $HOME/.nona/_plugin_scripts ! -path "$HOME/.nona/_plugin_scripts" -maxdepth 1 -type d \( ! -name . \) -exec bash -c "'{}'/activate.sh" \;
}

activate_plugins() {
  info "Activating all plugins\n\n"
  wp plugin activate --all 2>/dev/null
}

remove_sensitive_files() {
  THIS_BRANCH=$(git rev-parse --abbrev-ref HEAD)
  output "On the ${THIS_BRANCH} branch.\n"
  if
    [ ${THIS_BRANCH} = 'master' ]
  then
    if
      [ -f .deployignore ]
    then
      output "We have a deploy cleanup file, cleaning\n"
      for x in $(cat .deployignore); do
        rm -rf $x
      done
    else
      output "No cleanup file, skipping cleanup\n"
    fi
  else
    output "ONLY master can be cleanup up, aborting cleanup request\n"
  fi
}

activate_theme() {
  output "Activating Theme: ${THEME_NAME}\n"
  if
    # check for the theme
    [[ $(wp theme list  | grep "^${THEME_NAME}") ]]
  then
    if
      # check if it is inactive
      [[ $(wp theme list  | grep "^${THEME_NAME}" | grep "inactive") ]]
    then
      output "Theme is installed but not active\n"
      wp theme activate ${THEME_NAME}
      output "Activated Theme\n\n"
    else
      output "Theme is active\n\n"
    fi
  else
    output "Theme is not installed\n\n"
    exit 1
  fi
}

get_switches() {
  SWITCH_CLEANUP=0

  options=$@

  # An array with all the arguments
  arguments=($options)

  # Loop index
  index=0

  for argument in $options
  do
    # Incrementing index
    index=`expr $index + 1`

    # The conditions
    case $argument in
      --cleanup) SWITCH_CLEANUP=1 ;;
      --seed) SWITCH_SEED=1 ;;
    esac
  done
}

# seed the db if the seed flag is passed in
# this should never be run in prod
seed_database() {
  # get the siteurl
  url=$(wp option get siteurl)
  ~/.nona/bin/nonawp seed-db --force --domain ${url}
}

get_switches ${@}

import_config 1>/dev/null

activate_theme
run_plugin_scripts
activate_plugins 1>/dev/null

# allow a `--seed` flag that seed the db
if 
  [[ ${SWITCH_SEED} == 1 ]]
then
  seed_database
fi;

# allow a `--cleanup` flag that removes sensitive files
if 
  [[ ${SWITCH_CLEANUP} == 1 ]]
then
  remove_sensitive_files
fi;

output "\nCompleted deployment\n"
exit 0
