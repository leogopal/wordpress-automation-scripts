#!/bin/bash
source ~/.nona/_lib/utilities.sh
source ~/.nona/_lib/config.sh
source ~/.nona/_lib/wp-cli-utils.sh

generate_deploybot_script() {
  info "Copy the script below into your deploybot server commands box\n"
  message "-------------------------------------------------------------\n\n"
  cat ~/.nona/_deploy/deploybot.template | \
  sed "s/##THEME_NAME##/${THEME_NAME}/g" | \
  sed "s/##WP_TABLE_PREFIX##/${WP_TABLE_PREFIX}/g" | \
  sed "s/##AMAZON_S3_BUCKET##/${AMAZON_S3_BUCKET}/g" | \
  sed "s/##AWS_KEY##/${AWS_KEY}/g" | \
  sed "s/##AWS_SECRET##/${AWS_SECRET}/g" | \
  sed "s/##GRAVITY_FORMS_REGISTRATION_KEY##/${GRAVITY_FORMS_REGISTRATION_KEY}/g" | \
  sed "s/##OFFLOAD_S3_LICENSE##/${OFFLOAD_S3_LICENSE}/g" | \
  sed "s/##GITHUB_OAUTH_KEY##/${GITHUB_OAUTH_KEY}/g"
}

display_instructions() {
  info "Prerequisites\n\n"
  message "* Create a SSH server for your wordpress site on deploybot.\n"
  message "* Ensure your working directory is like '~/sites/<site-domain>/wp-content/themes'\n"
  message "* Confirm the login user is correct\n"
  message "* Make sure you are using public key authentication\n"
  message "* Make sure you have added the deploybot public key to the 'authorized_keys' file on Pagely\n"
  message "* Copy the output of this script (below the ------ line) to the deploybot commands box\n"
  info "\n"
  message "${purple}DO NOT RUN THE GENERATED SCRIPT ON YOUR DEV MACHINE\n"
  info "\n"
}

display_instructions
import_config
generate_deploybot_script

