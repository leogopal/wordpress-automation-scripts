# WordPress CLI Tool

### Problem

Our deploy scripts keep changing and are very out of sync between repos

### Solution

Centralise

# Installing

* Clone this repo
* Run the `install.sh` file
* Add `~/.nona/bin` to your $PATH
* Open a new terminal
* Use the `nonawp` tool

## Path

You must add the path to either your `~/.zshrc` or your `~/.bashrc` depending on
which shell you're using.

This usually means modifying your existing `PATH=""` to add it.

An example a without an existing export

```bash
# this is how you add paths in bash.
# The command basically says "Add ~/.nona/bin to the existing path"
export PATH="$PATH:$HOME/.nona/bin"
```

If you have an existing export add `:$HOME/.nona/bin` to the export. The `:` is 
important!

##### VVV Users

You need to

* `vagrant ssh`
* Clone this repo
* Run the installer
* Add `~/.nona/bin` to your $PATH
* Exit vagrant ssh session
* Enter vagrant ssh session again
* Run `nonawp` to confirm installation

# Using

You just type `nonawp <command>` from a valid WordPress install.

## Valid Commands

```
  prepare             Prepare a WordPress project. Useful after a fresh checkout.
  install-plugins     Installs the plugins defined in your wp-plugins.json file.
  deploybot-template  Generates a DeployBot deploy script based on your config.
  seed-db             Seeds your database from the `_seed` directory.
  pagely-deploy       Runs the pagely deployment script.
  self-update         Check and update this tool.
  generate-vhost      Outputs a vhost template for you to use with Apache.
  version             Show the version of this tool.
  help                This screen.
```

## Options

```
install-plugins
  --force       Forces install to run without prompting for confirmation.

seed-db
  --domain      Specify a domain to use. Removes need to prompt for hostname.
  --force       Forces a seed without prompting for confirmation.

pagely-deploy
  --seed        Force a seed of the database in the `_seed` folder.
  --cleanup     Forces a cleanup of sensitive files. This is dangerous. Don't do this locally.
```

## Pagely Server Requirements

From time to time the Pagely stack is rebuilt.

At this time we're working with Pagely to make sure these are always installed,
but here is the info on the tools, and details on how to install them without
root should you ever need to do so.

You place custom binaries and compile to your `~/bin` folder, and make sure that
the folder has been added to your $PATH

Your `~/.profile` file should contain something like this

```bash
# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
  PATH="$HOME/bin:$PATH"
  PATH="$HOME/bin/bin:$PATH" # an example additional folder
fi
```

### jq

* Download the (64bit) binary from [here](https://stedolan.github.io/jq/download/)
* Extract the archive
* Add the binary to your `~/bin` folder
* Make it executable with `chmod +x`

### node and npm

You need to install npm and node without root

```bash
mkdir ~/node-latest-install
cd ~/node-latest-install
curl http://nodejs.org/dist/node-latest.tar.gz | tar xz --strip-components=1
./configure --prefix=~
make install # take a while
curl https://www.npmjs.org/install.sh | sh
```

### bower

Install with `npm install -g bower`

### grunt

Install with `npm install -g grunt`

### composer

Composer is installed in the project every time due to the setup of the Pagely
server, so this doesn't need to be set up

### unzip

If this command is unavailable

```bash
mkdir ~/zip-install
cd ~/zip-install
wget http://downloads.sourceforge.net/infozip/unzip60.tar.gz
tar xzf unzip60.tar.gz
cd unzip60
make -f unix/Makefile generic
make prefix=~ -f unix/Makefile install
```

### other

* curl
* tar
* php (5.6+)

