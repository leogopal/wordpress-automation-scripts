#!/bin/bash
source ./nona/_lib/utilities.sh

SWITCH_FORCE=0
SWITCH_LINK=0

get_switches() {
  options=$@

  # An array with all the arguments
  arguments=($options)

  # Loop index
  index=0

  for argument in $options
  do
    # Incrementing index
    index=`expr $index + 1`

    # The conditions
    case $argument in
      --force) SWITCH_FORCE=1 ;;
      --link) SWITCH_LINK=1 ;;
    esac
  done
}

do_install() {
  if 
    [[ ${SWITCH_LINK} == 1 ]]
  then
    output "Symlinking this folder to ~/.nona. This should only be used when developing this tool.\n"
    ln -s $PWD/nona $HOME/.nona
    exit 0
  fi

  if 
    [[ -L "$HOME/.nona" ]]
  then
    warn "Installed as a symlink, aborting.\n"
    output "We cannot replace the symlink, please remove it first\n"
    output "Location: $HOME/.nona\n"
    exit 1
  fi

  if 
    [[ -e "$HOME/.nona" ]]
  then
    if
      [[ ${SWITCH_FORCE} == 0 ]]
    then
      if
        [[ $(cat $HOME/.nona/VERSION) == $(cat ./nona/VERSION) ]]
      then
        output "You already have the latest version installed.\n"
        exit 1
      fi
    fi

    output "Removing your existing Nona installation.\n"
    rm -rf "$HOME/.nona"
  fi
  output "Installing Nona WordPress CLI Tools\n"
  cp -rf ./nona ~/.nona
  mkdir ~/.nona/bin
  cp ~/.nona/nonawp.sh ~/.nona/bin/nonawp
  chmod +x ~/.nona/bin/nonawp
  output "Successfully Installed.\n\nYou need to ensure you have added \$HOME/.nona/bin to your path.\n"
  exit 0
}

get_switches ${@}
do_install

